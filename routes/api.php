<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * Route::middleware('auth:api')->get('/user', function (Request $request) {
 *   return $request->user();
 * });
*/


// Auth routes
$router->post('/login', 'Api\\Auth\\LoginController@login');

$router->resource('/ramais', 'Api\\BranchController')->only('index');
$router->resource('/links', 'Api\\LinkController')->only('index');
$router->resource('/configuracoes', 'Api\\SettingController')->only('index');
$router->resource('/informativos', 'Api\\InformativeController')
        ->middleware('auth:api')
        ->only('index');
$router->get('/agenda/disponibilidade', 'Api\\SchedulerController@checkAvailability');
$router->resource('/agenda', 'Api\\SchedulerController');
$router->resource('/estudantes', 'Api\\StudentController');

$router->post('/frequencias', 'Api\\FrequencyController@store')
        ->middleware('auth:api');

$router->post('/tickets', 'Api\\TicketController@send');

$router->group(['prefix' => '/admin', 'middleware' => 'auth:api'], function ($router) {
    $router->resource('/ramais', 'Api\\Admin\\BranchController', [
	    'as' => 'admin',
	])->parameters(['ramais' => 'branch']);

    $router->resource('/informativos', 'Api\\Admin\\InformativeController', [
	    'as' => 'admin',
	])->parameters(['informativos' => 'informative']);

    /*
    $router->resource('/links', 'Admin\\LinkController', [
	    'as' => 'admin',
	])->parameters(['links' => 'link']);

	$router->resource('/configuracoes', 'Admin\\SettingController', [
        'as' => 'admin',
	])->parameters(['configuracoes' => 'setting']);

    $router->resource('/usuarios', 'Admin\\UserController', [
        'as' => 'admin',
	])->parameters(['usuarios' => 'user'])
        ->only('index', 'edit', 'update');
     */
});

