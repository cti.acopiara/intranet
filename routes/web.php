<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')
    ->name('welcome');

Route::get('/home', function () {
    return redirect('/');
});

Auth::routes();

/*
 * Ramais
*/
$router->group(['prefix' => '/admin', 'middleware' => 'auth'], function ($router) {
    $router->resource('/ramais', 'Admin\\BranchController', [
	    'as' => 'admin',
	])->parameters(['ramais' => 'branch']);

    $router->resource('/informativos', 'Admin\\InformativeController', [
	    'as' => 'admin',
	])->parameters(['informativos' => 'informative']);

    $router->resource('/links', 'Admin\\LinkController', [
	    'as' => 'admin',
	])->parameters(['links' => 'link']);

	$router->resource('/configuracoes', 'Admin\\SettingController', [
        'as' => 'admin',
	])->parameters(['configuracoes' => 'setting']);

    $router->resource('/usuarios', 'Admin\\UserController', [
        'as' => 'admin',
	])->parameters(['usuarios' => 'user'])
        ->only('index', 'edit', 'update');
});

/*
 * Ramais
*/
$router->group(['prefix' => '/ramais'], function ($router) {
    $router->get('/', 'BranchesController@index')
        ->name('branches.branch.index');
});

/*
 * Frequencias
*/
$router->group(['prefix' => '/frequencias'], function ($router) {
    $router->get('/', 'FrequencyController@index')
        ->name('frequencies.frequency.index')
        ->middleware('auth');

    $router->post('/', 'FrequencyController@store')
        ->name('frequencies.frequency.store')
        ->middleware('auth');
});

/*
 * Informativos
*/
$router->group(['prefix' => 'informativos'], function($router) {
    $router->get('/', 'InformativeController@index')
        ->name('informatives.informative.index')
        ->middleware('auth');
});


$router->group(['prefix'=> 'tickets'], function($router) {
    $router->get('/', 'TicketsController@index')
        ->name('tickets.ticket.index');

    $router->post('/', 'TicketsController@send')
        ->name('tickets.ticket.send')
        ->where('sector', '[0-9]+');

    $router->get('/ti', 'TicketsController@ti')
        ->name('tickets.ticket.ti');

    $router->get('/infra', 'TicketsController@infra')
        ->name('tickets.ticket.infra');

});

/*
Route::group(
[
    'prefix' => 'tickets',
], function () {

    Route::get('/', 'TicketsController@index')
      ->name('tickets.ticket.index');

    Route::get('/ti', 'TicketsController@ti')
      ->name('tickets.ticket.ti');

    Route::get('/infra', 'TicketsController@infra')
      ->name('tickets.ticket.infra');

    Route::post('/', 'TicketsController@send')
      ->name('tickets.ticket.send')
      ->where('sector', '[0-9]+');

});
*/

