<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Informative;
use Illuminate\Http\Request;

class InformativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informatives = Informative::orderBy('published_at', 'DESC')->get();
        return response()->json($informatives);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function show(Informative $informative)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Informative $informative)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function destroy(Informative $informative)
    {
        //
    }
}
