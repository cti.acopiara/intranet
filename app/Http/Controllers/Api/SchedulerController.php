<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Scheduler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Spatie\GoogleCalendar\Event;

class SchedulerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nowPlus2Weeks = Carbon::now()->locale('pt-br')->addWeeks(2)->endOfDay();
		$events = Event::get($startDateTime = null, $endDateTime = $nowPlus2Weeks);
        return response()->json($events);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validateData($request);

        if ($validator->fails()) {
 			$messages = $validator->errors()->all();
            return response()->json($messages);
        }

        $startDateTime = Carbon::now('America/Fortaleza');
        $dtmp = explode("/", $request->eventDate);
        $ttmp = explode(":", $request->eventTime);

        $startDateTime->setDay((int)$dtmp[0]);
        $startDateTime->setMonth((int)$dtmp[1]);

        $startDateTime->setHours((int)$ttmp[0]);
        $startDateTime->setMinutes((int)$ttmp[1]);
        $startDateTime->setSeconds((int)$ttmp[2]);

        $endDateTime = $startDateTime->copy()->addMinutes(30);

        $eventName = sprintf("%s - %s", $request->name, $request->student_registration);
        $eventDescription = sprintf("Telefone de contato: %s", $request->phone_1);
        $event = Event::create([
            'name' => $eventName,
            'startDateTime' => $startDateTime,
            'endDateTime' => $endDateTime,
            'description' => $eventDescription,
            'attendees' => [
                ['email' => $request->email_personal]
            ],
        ]);
        return response()->json([$event, 'msg' => 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Scheduler  $scheduler
     * @return \Illuminate\Http\Response
     */
    public function show(Scheduler $scheduler)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Scheduler  $scheduler
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Scheduler $scheduler)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Scheduler  $scheduler
     * @return \Illuminate\Http\Response
     */
    public function destroy(Scheduler $scheduler)
    {
        //
    }

    /**
     * Check agenda availability for the next X days.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Scheduler  $scheduler
     * @return \Illuminate\Http\Response
     */
    public function checkAvailability(Request $request, Scheduler $scheduler)
    {
        if ($request->range == null) {
            $nowPlus1Day = Carbon::now()->locale('pt-br')->addDay()->startOfDay();
            $nowPlus2Weeks = Carbon::now()->locale('pt-br')->addWeeks(2)->endOfDay();
        }
		$events = Event::get($startDateTime = $nowPlus1Day, $endDateTime = $nowPlus2Weeks);
        $allOccupyTimes = array();
        foreach($events as $event) {
            $allOccupyTimes[] = $event->googleEvent->start->dateTime;
        }

        $next = Carbon::now()->tz('America/Fortaleza')->locale('pt-br');
        for ($i = 0; $i <= 14; $i++) {
            if (!$next->isWeekend()) {
                $next->hour = 8;
                $next->minute = 0;
                $next->second = 0;
                $allPossibleTimes[] = $next->copy()->toAtomString();
                for ($k = 0; $k <= 16; $k++) {
                    $next = $next->addMinutes(30);
                    if ($next->hour != 12) {
                        $allPossibleTimes[] = $next->copy()->toAtomString();
                    }
                }
            }
            $next->addDay();
        }

        $availableOptions = array_diff($allPossibleTimes, $allOccupyTimes);
        return response()->json($availableOptions);
    }



    protected function validateData(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'student_registration' => 'required|string',
            'eventDate' => 'required|string',
            'eventTime' => 'required|string'
        ];

        $validator = Validator::make($request->all(), $rules);

        return $validator;
    }

}
