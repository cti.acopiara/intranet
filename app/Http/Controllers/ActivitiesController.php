<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Activity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use Auth;
use Exception;

class ActivitiesController extends Controller
{

    /**
     * Display a listing of the activities.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $activities= Activity::paginate(25);
        return view('activities.index', compact('activities'));
    }

    /**
     * Show the form for creating a new activity.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        return view('activities.create');
    }

    /**
     * Store a new activity in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
          $user = Auth::user();
          $localtime = Carbon::now('America/Fortaleza');

          if ($request->hasFile('doc')) {
            $file = $request->file('doc'); 
            $filename = $user->login . '-' . $localtime->timestamp;
            $ext = $file->getClientOriginalExtension();
            Storage::disk('public')->put($filename.'.'.$ext,  File::get($file));
          }

            $data = $this->getData($request);
            $url = Storage::url($filename.'.'.$ext);
            $activity = Activity::create([
              'user_id' => Auth::user()->id,
              'type' => $data['type'],
              'description' => $data['description'],
              'workload' => $data['workload'],
              'course' => $data['course'],
              'semester' => $data['semester'],
              'meta' => [
                'doc_count' => 1,
                'doc_url' => $url,
              ]
            ]);

            return redirect()->route('teachers.teacher.index')
                             ->with('success_message', 'Atividade adicionada com sucesso.');

        } catch (Exception $exception) {

            return back()->withInput()
              ->withErrors([
                'semester_error' => 'O semestre deve ser preenchido no formato "XXXX.Y", exemplo: 2018.1',
                'file_error' => 'O arquivo comprovando a atividade deve está no formato PDF.'
              ]);
        }
    }

    /**
     * Display the specified activity.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $activity = Activity::findOrFail($id);
        return view('activities.show', compact('activity'));
    }

    /**
     * Show the form for editing the specified activity.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $activity = Activity::findOrFail($id);
        return view('activities.edit', compact('activity'));
    }

    /**
     * Update the specified activity in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        //try {

          $user = Auth::user();
          $localtime = Carbon::now('America/Fortaleza');

          if ($request->hasFile('doc')) {
            $file = $request->file('doc'); 
            $filename = $user->login . '-' . $localtime->timestamp;
            $ext = $file->getClientOriginalExtension();
            Storage::disk('public')->put($filename.'.'.$ext,  File::get($file));
            $url = Storage::url($filename.'.'.$ext);
            $data = $this->getData($request);
            $data['meta']['doc_url'] = $url;
          }
            
          $data = $this->getData($request);
          $activity = Activity::findOrFail($id);
          $activity->update($data);

          return redirect()->route('teachers.teacher.index')
                             ->with('success_message', 'Atividade atualizada com sucesso.');

        //} catch (Exception $exception) {

         //   return back()->withInput()
         //                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        //}        
    }

    /**
     * Remove the specified activity from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $activity = Activity::findOrFail($id);

            $doc_url = $activity->meta['doc_url'];
            Storage::disk('public')->delete($doc_url);
            $activity->delete();

            return redirect()->route('teachers.teacher.index')
                             ->with('success_message', 'Atividade removida com sucesso.');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'type' => 'required|string|min:1|max:191',
            'description' => 'required|string|min:1',
            'course' => 'required|string|min:1',
            'semester' => 'required|string|min:1',
            'workload' => 'required|between:0,99.99',
     
        ];
        
        $data = $request->validate($rules);


        return $data;
    }

}
