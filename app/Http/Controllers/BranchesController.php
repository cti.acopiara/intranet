<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class BranchesController extends Controller
{

    /**
     * Display a listing of the branches.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $branches = Branch::orderBy('branch_line', 'ASC')->get();
        return view('branches.index', compact('branches'));
    }

    /**
     * Show the form for creating a new branch.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        return view('admin.branches.create');
    }

    /**
     * Store a new branch in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            Branch::create($data);

            return redirect()->route('branches.branch.index')
                             ->with('success_message', 'Branch was successfully added!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    /**
     * Display the specified branch.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $branch = Branch::findOrFail($id);

        return view('branches.show', compact('branch'));
    }

    /**
     * Show the form for editing the specified branch.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $branch = Branch::findOrFail($id);
        

        return view('branches.edit', compact('branch'));
    }

    /**
     * Update the specified branch in the storage.
     *
     * @param  int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $branch = Branch::findOrFail($id);
            $branch->update($data);

            return redirect()->route('branches.branch.index')
                             ->with('success_message', 'Branch was successfully updated!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }        
    }

    /**
     * Remove the specified branch from the storage.
     *
     * @param  int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $branch = Branch::findOrFail($id);
            $branch->delete();

            return redirect()->route('branches.branch.index')
                             ->with('success_message', 'Branch was successfully deleted!');

        } catch (Exception $exception) {

            return back()->withInput()
                         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request!']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'sector' => 'required|string|min:1|max:191',
            'contacts' => 'required|string|min:1|max:191',
            'branch_line' => 'required|string|min:1|max:191',
     
        ];
        
        $data = $request->validate($rules);


        return $data;
    }

}
