<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'required|string|max:10|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      if ($data['user_type'] == 'docente') {

        $user = User::create([
          'name' => $data['name'],
          'user_type' => $data['user_type'],
          'username' => $data['username'],
          'email' => $data['email'],
          'password' => bcrypt($data['password']),
          'meta' => [
            'modules' => 'docente',
            'birthday' => $data['birthday'],
            'phone' => $data['phone'],
            'workload_in_semester' => $data['workload_in_semester'],
            'workload' => $data['workload'],
            'regime' => $data['regime'],
            //'exclusive_dedication' => $data['exclusive_dedication'],
            'responsability' => $data['responsability'],
            'department' => $data['department']
          ]
        ]);
      } else if ($data['user_type'] == 'tae') {
        $user = User::create([
          'name' => $data['name'],
          'user_type' => $data['user_type'],
          'username' => $data['username'],
          'email' => $data['email'],
          'password' => bcrypt($data['password']),
          'meta' => [
            'modules' => 'tae',
            'birthday' => $data['birthday'],
            'phone' => $data['phone'],
            'work_schedule' => $data['work_schedule'],
            'responsability' => $data['responsability'],
            'sector' => $data['sector']
          ]
        ]);
      }
      return $user;

    }
}
