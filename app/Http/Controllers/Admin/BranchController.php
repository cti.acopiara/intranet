<?php

namespace App\Http\Controllers\Admin;

use App\Models\Branch;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::orderBy('branch_line', 'ASC')->get();
        return view('admin.branches.index', compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.branches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validateData($request);

        if ($validator->fails()) {
				    return back()
            		->withInput()
								->withErrors($validator->messages());
        }

        Branch::create($request->all());
        return redirect()->route('admin.ramais.index')
            ->with('success_message', 'Ramal adicionado com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        return view('admin.branches.show', compact('branch'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit(Branch $branch)
    {
        $branch = Branch::findOrFail($branch->id);
        return view('admin.branches.edit', compact('branch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {
        $validator = $this->validateData($request);

        if ($validator->fails()) {
				    return back()
            		->withInput()
								->withErrors($validator->messages());
        }

        $branch->update($request->all());

        return redirect()->route('admin.ramais.index')
            ->with('success_message', 'Ramal atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        $branch->delete();
        return redirect()->route('admin.ramais.index')
            ->with('success_message', 'Ramal removido com sucesso.');
    }

    protected function validateData(Request $request)
    {
        $rules = [
            'sector' => 'required|string|min:1|max:191',
            'contacts' => 'required|string|min:1|max:191',
            'branch_line' => 'required|string|min:1|max:4',
        ];

        $validator = Validator::make($request->all(), $rules);

        return $validator;
    }

}
