<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Informative;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class InformativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informatives = Informative::orderBy('published_at', 'ASC')->get();
        return view('admin.informatives.index', compact('informatives'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.informatives.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $aux = Carbon::createFromFormat('d/m/Y', $request->published_at, 'America/Fortaleza');
        } catch (Exception $exception) {
            $aux = Carbon::createFromFormat('Y/m/d h:mm:ss', $request->published_at, 'America/Fortaleza');
        }
        $request->merge(['published_at' => $aux]);

        $validator = $this->validateData($request);

        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator->messages());
        }

        Informative::create($request->all());
        return redirect()->route('admin.informativos.index')
            ->with('success_message', 'Informativo adicionado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function show(Informative $informative)
    {
        return view('admin.informatives.show', compact('informative'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function edit(Informative $informative)
    {
        $informative = Informative::findOrFail($informative->id);
        return view('admin.informatives.edit', compact('informative'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Informative $informative)
    {
        $validator = $this->validateData($request);

        if ($validator->fails()) {
		    return back()
                ->withInput()
                ->withErrors($validator->messages());
        }

        $informative->update($request->all());

        return redirect()->route('admin.informativos.index')
            ->with('success_message', 'Informativo atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Informative  $informative
     * @return \Illuminate\Http\Response
     */
    public function destroy(Informative $informative)
    {
        $informative->delete();
        return redirect()->route('admin.informativos.index')
            ->with('success_message', 'Informativo removido com sucesso.');
    }

    protected function validateData(Request $request)
    {
        $rules = [
            'title' => 'required|string|min:1|max:191',
            'link' => 'required|string|min:1',
            'published_at' => 'required|date',
        ];

        $validator = Validator::make($request->all(), $rules);

        return $validator;
    }

}
