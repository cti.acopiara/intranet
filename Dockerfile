# build-stage
FROM composer:1.10.5 as build-stage

WORKDIR /app

COPY composer.lock composer.json /app/
COPY database /app/database/

RUN composer install \
    --no-ansi \
    --no-dev \
    --no-interaction \
    --no-plugins \
    --no-progress \
    --no-scripts \
    --no-suggest \
    --optimize-autoloader \
    --ignore-platform-reqs

COPY . .

FROM php:7.4.5-fpm-alpine

RUN apk update && apk add build-base \
    curl-dev \
    libmcrypt-dev \
    libxml2-dev \
    libpng-dev \
    libzip-dev \
    postgresql-dev \
    openldap-dev \
    libcurl \
    git \
    && docker-php-ext-install curl dom gd pdo_pgsql pgsql xml zip ldap

  # libcurl4-openssl-dev (debian package)
  # libldap2-dev (debian packge)
  # libpqtypes-dev (debian package)

WORKDIR /app

COPY --from=build-stage --chown=www-data:www-data /app/. .
RUN rm -f /app/bootstrap/cache/*.php \
    && chmod -R 777 /app/storage/ \
    && php artisan clear-compiled

