@extends('layouts.modules')

@section('content')

<div class="uk-section">
  <div class="uk-container uk-container-center">
    <div class="uk-width-1-2@m uk-align-center">
      <div class="uk-card uk-card-body uk-card-small">
        <h2>Cadastrar nova atividade</h2>
          @if ($errors->any())
            <div class="uk-card uk-card-body uk-card-secondary uk-margin">
              <h4 class="uk-card-title">Verifique se o formulário está correto</h4>
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

        <form method="POST" action="{{ route('activities.activity.store') }}" accept-charset="UTF-8" id="create_activity_form" name="create_activity_form" enctype="multipart/form-data">
          {{ csrf_field() }}
          @include ('activities.form', ['activity' => null])

          <input class="uk-button uk-button-primary" type="submit" value="Adicionar">

        </form>

      </div>
    </div>
  </div>
</div>

@endsection

