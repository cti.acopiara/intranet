@extends('layouts.modules')

@section('content')

<div class="uk-section">
  <div class="uk-container uk-container-center">

    <h2>Atividades</h2>

  </div> <!-- uk-container -->
</div> <!-- /uk-section -->
@endsection


@section('jscontent')
  <script>
  @if(Session::has('success_message'))
    UIkit.notification({
      message: '{!! session('success_message') !!}',
      status: 'primary',
      pos: 'bottom-right',
      timeout: 5000
    });
  @endif

  </script>
@endsection

