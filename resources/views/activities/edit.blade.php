@extends('layouts.modules')

@section('content')

<div class="uk-section">
  <div class="uk-container uk-container-center">
    <div class="uk-width-1-2@m uk-align-center">
      <div class="uk-card uk-card-body uk-card-small">
        <h2>Editar atividade</h2>
          @if ($errors->any())
            <div class="uk-card uk-card-body uk-card-secondary uk-margin">
              <h4 class="uk-card-title">Verifique se o formulário está correto</h4>
              <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
              </ul>
            </div>
          @endif

        <form method="POST" action="{{ route('activities.activity.update', $activity->id) }}" id="edit_activity_form" name="edit_activity_form" enctype="multipart/form-data" accept-charset="UTF-8">
          {{ csrf_field() }}
          <input name="_method" type="hidden" value="PUT">
          @include ('activities.form', ['activity' => $activity])

          <input class="uk-button uk-button-default" type="submit" value="Atualizar">
        </form>

      </div>
    </div>
  </div>
</div>

@endsection
