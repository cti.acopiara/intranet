@extends('layouts.modules')

@section('content')

<div class="uk-section">
  <div class="uk-container uk-container-center">
    <div class="uk-width-1-2@m uk-align-center">
      <div class="uk-card uk-card-body uk-card-small">
        <h1 class="uk-card-title">Atividade - {{ $activity->type }}</h1>
        <ul class="uk-list uk-list-striped">
          <li><strong>Descrição:</strong> {{ $activity->description }}</li>
          <li><strong>Curso:</strong> {{ $activity->course }}</li>
          <li><strong>Semestre:</strong> {{ $activity->semester }}</li>
          <li><strong>Carga horária:</strong> {{ number_format($activity->workload, 2, ',', '.') }} h</li>
          <li><strong>Arquivo comprobatório:</strong> <a href="{{ URL::to($activity->meta['doc_url']) }}" target="_blank">Clique aqui</a></li>
          <li><strong>Criado em:</strong> {{ $activity->created_at }}</li>
          <li><strong>Atualizado em:</strong> {{ $activity->updated_at }}</li>
        </ul>

        <a class="uk-button uk-button-default" href="javascript:history.back()">Voltar</a>
      </div>
    </div>
  </div>
</div>

@endsection
