<div>

  <label class="uk-form-label" for="type">Tipo da atividade</label>
  <select id="type" class="uk-select" name="type" required>
    <option value="FIC">FIC</option>
    <option value="Manutenção ao ensino">Manutenção ao ensino</option>
    <option value="Apoio ao ensino">Apoio ao ensino</option>
    <option value="Orientação">Orientação</option>
    <option value="Ensino extracurricular">Ensino extracurricular</option>
    <option value="Pesquisa aplicada">Pesquisa aplicada</option>
    <option value="Extensão">Extensão</option>
    <option value="Gestão">Gestão</option>
    <option value="Comissões ou Fiscalização">Comissões ou Fiscalização</option>
  </select>

  <div class="uk-margin">
  <label class="uk-form-label" for="description">Descrição</label>
  <div class="uk-form-controls">
    <input class="uk-input {{ $errors->has('description') ? 'uk-form-danger' : '' }}" name="description" type="text" id="description" value="{{ old('description', optional($activity)->description) }}" minlength="10" maxlength="190" required="true" placeholder="Descrição da atividade">
    {!! $errors->first('description', '<p class="uk-text-small uk-text-danger">:message</p>') !!}
  </div>

  <div class="uk-margin">
  <label class="uk-form-label" for="workload">Carga horária</label>
  <div class="uk-form-controls">
    <input class="uk-input {{ $errors->has('workload') ? 'uk-form-danger' : '' }}" name="workload" type="text" id="workload" value="{{ old('workload', optional($activity)->workload) }}" minlength="1" maxlength="6" required="true" placeholder="Carga horária da atividade">
    {!! $errors->first('workload', '<p class="uk-text-small uk-text-danger">:message</p>') !!}
  </div>
  </div>

  <div class="uk-margin">
  <label class="uk-form-label" for="course">Curso</label>
  <div class="uk-form-controls">
    <input class="uk-input {{ $errors->has('course') ? 'uk-form-danger' : '' }}" name="course" type="text" id="course" value="{{ old('course', optional($activity)->course) }}" minlength="1" maxlength="30" required="true" placeholder="Curso">
    {!! $errors->first('course', '<p class="uk-text-small uk-text-danger">:message</p>') !!}
  </div>
  </div>

  <div class="uk-margin">
  <label class="uk-form-label" for="semester">Semestre</label>
  <div class="uk-form-controls">
    <input class="uk-input {{ $errors->has('semester') ? 'uk-form-danger' : '' }}" name="semester" type="text" id="semester" value="{{ old('semester', optional($activity)->semester) }}" minlength="1" maxlength="30" required="true" placeholder="Semestre">
    {!! $errors->first('semester', '<p class="uk-text-small uk-text-danger">:message</p>') !!}
  </div>
  </div>

  <div class="uk-margin" uk-margin>
  <label class="uk-form-label" for="semester">Documento comprobatório <span class="uk-text-small uk-text-warning"> (Apenas documento no formato PDF)</span></label>
    <div uk-form-custom="target: true">
      <input type="file" name="doc"accept="application/pdf">
      <input class="uk-input" type="text" placeholder="Clique para escolher.." disabled>
    </div>
  </div>

</div>
