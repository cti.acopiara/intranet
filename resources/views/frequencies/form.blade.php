<div>

  <div class="columns">
    <div class="column">

    <p class="content">
      {{ $user->name }} ({{ $user->username }})<br />
      {{ $user->responsability }} ({{ $user->sector }})
    </p>

    <input value="{{ $user->name }}" name="name" type="hidden">
    <input value="{{ $user->username }}" name="username" type="hidden">
    <input value="{{ $user->responsability }}" name="responsability" type="hidden">
    <input value="{{ $user->sector }}" name="sector" type="hidden">

    <div class="field">
      <label class="label" for="role">Função</label>
      <input id="role" name="role" class="input " type="text" value=""> 
      <span class="help">Caso não tenha, deixar em branco</span>
    </div>

    <div class="field">
      <div class="control is-expanded">
        <label class="label" for="month">Mês</label>
        <span class="select is-fullwidth">
        <select id="month" name="month" class="">
          <option value="1">Janeiro</option>
          <option value="2">Fevereiro</option>
          <option value="3">Março</option>
          <option value="4">Abril</option>
          <option value="5">Maio</option>
          <option value="6">Junho</option>
          <option value="7">Julho</option>
          <option value="8">Agosto</option>
          <option value="9">Setembro</option>
          <option value="10">Outubro</option>
          <option value="11">Novembro</option>
          <option value="12">Dezembro</option>
        </select>
        </span>
      </div>
    </div>

    <div class="field">
      <div class="control is-expanded">
      <label class="label" for="Ano">Ano</label>
      <span class="select is-fullwidth">
        <select id="year" name="year" class="">
          <option value="2020">2020</option>
          <option value="2021">2021</option>
          <option value="2022">2022</option>
          <option value="2023">2023</option>
        </select>
      </span>
      </div>
    </div>

    <div class="field">
      <label class="label" for="role">Horário de trabalho</label>
      <input id="work_schedule" name="work_schedule" class="input " type="text" value="{{ $user->work_schedule }}"> 
    </div>

  <div class="field">
    <label class="label">Jornada de trabalho semanal</label>
    <label><input class="uk-radio" type="radio" name="work_load" value="40" checked onchange="toggleShift(this)"> 40 horas</label>
    <label><input class="uk-radio" type="radio" name="work_load" value="30" onchange="toggleShift(this)"> 30 horas</label>
  </div>

  <div id="form-shift" hidden>
  <div class="field">
    <label class="label">Turno</label>
    <label><input class="uk-radio" type="radio" name="work_shift" value="m" checked> Manhã</label>
    <label><input class="uk-radio" type="radio" name="work_shift" value="t"> Tarde</label>
  </div>
  </div> <!-- shift-form -->

  <div class="field">
    <label class="label">Férias?</label>
    <label><input class="uk-radio" type="radio" name="vacation" value="n" onchange="toggleShift(this)" checked> Não</label>
    <label><input class="uk-radio" type="radio" name="vacation" value="s" onchange="toggleShift(this)"> Sim</label>
  </div>

  <div id="form-vacation" hidden>
  <div class="columns">

    <div class="column">
      <label class="label" for="start-vacation">Início</label>
        <div class="control">
          <input id="vacation-1" name="start-vacation" class="input " type="text" value=""> 
          <span class="help">Data no formato DD/MM/AAAA.</span>
        </div>
    </div>

    <div class="column">
      <label class="label" for="end-vacation">Término</label>
        <div class="control">
          <input id="vacation-2" name="end-vacation" class="input " type="text" value=""> 
          <span class="help">Data no formato DD/MM/AAAA.</span>
        </div>
    </div>

  </div> <!-- vacation-form -->

    </div> <!-- column -->
  </div> <!-- columns -->

</div>

@section('jscontent')
<script>
  function toggleShift(that) {
    if (that.value == "30") {
      document.getElementById("form-shift").removeAttribute("hidden");
    } else if (that.value == "40") {
      document.getElementById("form-shift").setAttribute("hidden", true);
    } else if (that.value == "s") {
      document.getElementById("form-vacation").removeAttribute("hidden");
    } else if (that.value == "n") {
      document.getElementById("form-vacation").setAttribute("hidden", true);
    }
  }

  </script>
@endsection
