  <div class="columns">
    <div class="column">

    <input id="queue" name="queue" class="input" type="hidden" value="ti">
    <p class="content">
      @if (!Auth::guest())
        <strong>Chamado aberto por:</strong><br />
        {{ Auth::user()->name }} ({{ Auth::user()->username }}) - {{ Auth::user()->sector }} <br />
        {{ Auth::user()->email }}
        <input name="name" class="input" type="hidden" value="{{ Auth::user()->name }}">
        <input name="login" class="input" type="hidden" value="{{ Auth::user()->login }}">
        <input name="email" class="input" type="hidden" value="{{ Auth::user()->email }}">
        <input name="sector" class="input" type="hidden" value="{{ Auth::user()->sector }}">

      @else
        <div class="field">
          <label class="label" for="name">Nome</label>
          <input id="name" name="name" class="input" type="text" value="">
        </div>

        <div class="field">
          <label class="label" for="login">SIAPE ou Matrícula</label>
          <input id="login" name="login" class="input" type="text" value="">
        </div>

        <div class="field">
          <label class="label" for="sector">Setor</label>
          <input id="sector" name="sector" class="input" type="text" value="">
        </div>

        <div class="field">
          <label class="label" for="email">E-mail</label>
          <input id="email" name="email" class="input" type="email" value="">
        </div>
      @endif
    </p>

    <input id="emailto" name="emailto" class="input" type="hidden" value="suporte.quixada@ifce.edu.br">
    <div class="field">
      <div class="control is-expanded">
        <label class="label" for="subject">Assunto</label>
        <span class="select is-fullwidth">
        <select id="subject" name="subject" class="">
            <option selected disabled>Selecione uma opção</option>
            <option value="Solicitar remoção/inclusão de software">Solicitar remoção/inclusão de software</option>
            <option value="Solicitar remoção/inclusão de periférico (teclado, mouse..)">Solicitar remoção/inclusão de periférico (teclado, mouse..)</option>
            <option value="Solicitar novo tonner de impressora">Solicitar novo tonner de impressora</option>
            <option value="Solicitar novo ponto de rede">Solicitar novo ponto de rede</option>
            <option value="Informar mau funcionamento em equipamento (pc, impressora..)">Informar problema em equipamento (pc, impressora..)</option>
            <option value="Outros">Outros</option>
        </select>
        </span>
      </div>
    </div>

    <div class="field">
      <label class="label" for="number">Tombo do equipamento</label>
      <input id="number" name="number" class="input " type="text" value="">
      <span class="help">Caso não tenha, deixar em branco</span>
    </div>


    <div class="field">
      <label class="label" for="content">Descreva o problema/requisição aqui</label>
      <div class="control">
        <textarea id="content" name="content" class="textarea" placeholder=""></textarea>
      </div>
    </div>


    </div> <!-- column -->
  </div> <!-- columns -->

@section('jscontent')
<script>
</script>
@endsection
