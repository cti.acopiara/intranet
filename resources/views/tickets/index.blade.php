@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-half">
      <div class="box">
        <h2 class="title has-text-centered">Abrir chamado</h2>
    
        <div class="columns">
          <div class="column has-text-centered">
            <a class="" href="{{ route('tickets.ticket.ti') }}">
            <div class="box is-shadowless">
              <span class="icon is-large">
                <i class="mdi mdi-48px mdi-laptop"></i>
              </span>
              <h4>Tecnologia da Informação</h4>
            </div>
            </a>
          </div> <!-- column -->

          <div class="column has-text-centered">
            <a class="" href="{{ route('tickets.ticket.infra') }}">
            <div class="box is-shadowless">
              <span class="icon is-large">
                <i class="mdi mdi-48px mdi-wrench-outline"></i>
              </span>
              <h4>Infraestrutura e Transportes</h4>
            </div>
            </a>
          </div> <!-- column -->
        </div> <!-- columns -->

        <a class="button is-white" href="{{ route('welcome') }}">Voltar para página inicial</a>
      </div>
      </div> <!-- uk-card -->
    </div> <!-- uk-width -->
  </div> <!-- uk-container -->
</div> <!-- /uk-section -->
@endsection


@section('jscontent')
  <script>
  @if(Session::has('success_message'))
    UIkit.notification({
      message: '{!! session('success_message') !!}',
      status: 'primary',
      pos: 'bottom-right',
      timeout: 5000
    });
  @endif

  </script>
@endsection

