@extends('layouts.frequency')

@section('meta')
<style>
  body {
    font-family: 'Roboto Condensed', sans-serif;
    font-size: 10pt;
    margin-top: 2cm;
    margin-right: 2cm;
    margin-bottom: 1.5cm;
    margin-left: 3cm;
    width: auto;
    margin: 0 auto;
    padding: 0;
    padding-left: 30px;
    padding-right: 30px;
    float: none !important;
  }
  h1, h2 {
    font-family: 'Roboto Condensed', sans-serif;
  }
  .page-break {
    page-break-after: always;
  }
  #days {
    font-size: 7pt;
    margin-bottom: 0.6cm; 
  } 
  #ocurrencies {
    margin-bottom: 0.6cm;
  }
  #info {
    margin-bottom: 0.4cm; 
    border-collapse: collapse;
    border-spacing: 0;
  }
  #header {
    font-size: 8pt;
    margin-bottom: 0.2cm;
    border-collapse: collapse;
    border-spacing: 0;
  }
  h1, h2, h4 {
    text-align: center;
    margin: 0;
  }
</style>
@endsection

@section('content')

  <div id="frequency-content">
  <div> 
    <div>

      <table width="100%" id="header" border="0">
        <tr>
          <td>
            <img src="{{ public_path() . '/img/logo-ifce.png' }}" alt="logo ifce" width="200" />
          </td>
          <td>
            <h2>ORDEM DE SERVIÇO - INFRAESTUTURA</h2>
            <h4>CAMPUS {{ strtoupper(config('app.campus', 'Fortaleza')) }}</h4>
          </td>
        </tr>
      </table>


      <table width="100%" id="info" border="0" color="black" cellpadding="0px">
        <tr>
          <td align="left" colspan="3"><strong>REQUISITANTE:</strong> {{ $data['name'] }}</td>
          <td align="left" colspan="2"><strong>SIAPE:</strong> {{ $data['login'] }}</td>
          </tr>
        <tr>
          <td align="left" colspan="3"><strong>CHEFIA IMEDIATA:</strong> {{ $data['chief'] }}</td>
          <td align="left" colspan="2"><strong>DATA REQUISIÇÃO:</strong> {{ $dt_request->format('d/m/y - H:i') }}</td>
        </tr>
        <tr>
          <td align="left" colspan="2"><strong>SETOR:</strong> {{ $data['sector'] }}</td>
          <td align="left"></td>
          <td align="left" colspan="2"><strong>NUM. OS:</strong> </td>
        </tr>
      </table>

      <table width="100%" id="days" border="1" color="black" cellspacing="20px" cellpadding="2px" style="border-collapse:collapse;border-spacing:0;"> 
        <tr>
        </tr>
      </table>

      <h2>SOLICITAÇÃO DE SERVIÇOS</h2>
      <table width="100%" id="request-content" border="0" cellpadding="10px">
        <tr>
          <td colspan="1" style="border:1px solid black;"><strong>{{ $data['content'] }}</strong></td>
        </tr>
      </table>

      <h2>EXECUÇÃO</h2>
      <table width="100%" id="ps-content" border="0" cellpadding="10px">
        <tr>
          <td colspan="1" style="border:1px solid black;"><strong>Observações:</strong></td>
        </tr>
        @for ($k = 1; $k <= 10; $k++)
          <tr>
            <td></td>
          </tr>
        @endfor
      </table>

      <table width="40%" id="footer-left" border="0" cellpadding="10px">
        <tr>
          <td colspan="1" width="8cm" style="border:1px solid black;"><strong>DATA E HORA DE INÍCIO:</strong></td>
        </tr>
      </table>    
      <table width="40%" id="footer-right" border="0" cellpadding="10px">
        <tr>
          <td colspan="1" width="8cm" style="border:1px solid black;"><strong>DATA E HORA DE TÉRMINO:</strong></td>
        </tr>
      </table>

    </div> 
  </div> 
  </div>

@endsection

