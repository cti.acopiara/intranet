<div id="form-administrativo" hidden>
  <div class="">

  <div class="field">
    <label class="label">SIAPE</label>
    <input 
      id="administrativo-login" 
      type="text" 
      class="input {{ $errors->has('username') ? ' is-danger' : '' }}" 
      name="username" 
      value="{{ old('username') }}" 
      required>

      @if ($errors->has('username'))
        <p class="help is-danger">
          {{ $errors->first('username') }}
        </p>
      @endif
  </div> <!-- field administrativo-login -->

  <div class="field">
    <label class="label">Nome completo</label>
    <input 
      id="administrativo-name" 
      type="text" 
      class="input {{ $errors->has('name') ? ' is-danger' : '' }}" 
      name="name" 
      value="{{ old('name') }}" 
      required>

      @if ($errors->has('name'))
        <p class="help is-danger">
          {{ $errors->first('name') }}
        </p>
      @endif
  </div> <!-- field administrativo-name -->

  <div class="field">
    <label class="label">Cargo</label>
    <input 
      id="administrativo-responsability" 
      type="text" 
      class="input {{ $errors->has('responsability') ? ' is-danger' : '' }}" 
      name="responsability" 
      value="{{ old('responsability') }}" 
      required>

      @if ($errors->has('responsability'))
        <p class="help is-danger">
          {{ $errors->first('responsability') }}
        </p>
      @endif
  </div> <!-- field administrativo-responsability -->

  <div class="field">
    <label class="label">Data de aniversário</label>
    <input 
      id="administrativo-birthday" 
      type="text" 
      class="input {{ $errors->has('birthday') ? ' is-danger' : '' }}" 
      name="birthday" 
      value="{{ old('birthday') }}" 
      required>

    @if ($errors->has('birthday'))
      <p class="help is-danger">
        {{ $errors->first('birthday') }}
      </p>
    @endif
  </div> <!-- field administrativo-birthday -->

  <div class="field">
    <label class="label">Telefone</label>
    <input 
      id="administrativo-phone" 
      type="text" 
      class="input {{ $errors->has('phone') ? ' is-danger' : '' }}" 
      name="phone" 
      value="{{ old('phone') }}" 
      required>

    @if ($errors->has('phone'))
      <p class="help is-danger">
        {{ $errors->first('phone') }}
      </p>
    @endif
  </div> <!-- field administrativo-phone -->

  <div class="field">
    <label class="label">Setor</label>
    <input 
      id="administrativo-sector" 
      type="text" 
      class="input {{ $errors->has('sector') ? ' is-danger' : '' }}" 
      name="sector" 
      value="{{ old('sector') }}" 
      required>

    @if ($errors->has('sector'))
      <p class="help is-danger">
        {{ $errors->first('sector') }}
      </p>
    @endif
  </div> <!-- field administrativo-work_schedule -->

  <div class="field">
    <label class="label">Horário de trabalho</label>
    <div class="control">
      <input 
        id="administrativo-work_schedule" 
        type="text" 
        class="input {{ $errors->has('sector') ? ' is-danger' : '' }}" 
        name="work_schedule" 
        value="SEG-SEX: 08h - 12h / 13h - 17h" 
        required>
    </div>

    @if ($errors->has('sector'))
      <p class="help is-danger">
        {{ $errors->first('work_schedule') }}
      </p>
    @endif

    <p class="help">Opções de modo de preenchimento: 
      <ul class="is-size-7">
        <li>SEG: 09h - 12h / 13h - 18h TER-SEX: 07h - 12h / 13h - 16h</li>
        <li>SEG: 09h - 12h / 13h - 18h TER-QUI: 08h - 12h / 13h - 17h SEX: 07h - 12h / 13h - 16h</li>
        <li>SEG-QUA-SEX: 09h - 12h / 13h - 18h TER-QUI: 07h - 12h / 13h - 16h</li>
      </ul>
    </p>

  </div> <!-- field administrativo-sector -->



  <div class="field">
    <label class="label">Email</label>
    <input 
      id="administrativo-email" 
      type="email" 
      class="input {{ $errors->has('email') ? ' is-danger' : '' }}" 
      name="email" 
      value="{{ old('email') }}" 
      required>

    @if ($errors->has('email'))
      <p class="help is-danger">
        {{ $errors->first('email') }}
      </p>
    @endif
  </div> <!-- field administrativo-email -->

  <div class="field">
    <label class="label">Senha</label>
    <input 
      id="administrativo-password" 
      type="password" 
      class="input {{ $errors->has('password') ? ' is-danger' : '' }}" 
      name="password" 
      value="{{ old('password') }}" 
      required>

    @if ($errors->has('password'))
      <p class="help is-danger">
        {{ $errors->first('password') }}
      </p>
    @endif
  </div> <!-- field administrativo-password -->

  <div class="field">
    <label class="label">Confirmar senha</label>
    <input 
      id="administrativo-password_confirmation" 
      type="password" 
      class="input {{ $errors->has('password_confirmation') ? ' is-danger' : '' }}" 
      name="password_confirmation" 
      value="{{ old('password_confirmation') }}" 
      required>

    @if ($errors->has('password_confirmation'))
      <p class="help is-danger">
        {{ $errors->first('password_confirmation') }}
      </p>
    @endif
  </div> <!-- field administrativo-password_confirmation -->

  <div class="field">
    <button class="button is-primary" type="submit" name="button">Registrar</button>
    <a class="help" href="{{ route('login') }}">
      Já tem conta? Acesse aqui.
    </a>
  </div> <!-- field button -->

  </div> <!-- uk-animation-fade -->
</div> <!-- form-administrativo -->



