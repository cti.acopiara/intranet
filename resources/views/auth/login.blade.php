@extends('layouts.modules')

@section('content')
  <div class="section">
    <div class="container">

      <div class="columns is-centered">
      <div class="column is-two-thirds">

        <div class="box">
          <div class="columns">
            <div class="column">
              <h2 class="title has-text-centered">Acessar sistemas internos</h2>
            </div>
          </div>
          <div class="columns">

            <div class="column">
              <form class="form" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="field">
                  <label class="label">Siape ou matrícula</label>
                  <div class="control">
                    <input 
                      id="username" 
                      type="text" 
                      class="input {{ $errors->has('username') ? ' is-danger' : '' }}" 
                      name="username" 
                      value="{{ old('username') }}" 
                      required 
                      autofocus>
                  </div>

                  @if ($errors->has('username'))
                    <p class="help">
                      {{ $errors->first('username') }}
                    </p>
                  @endif
                </div>

                <div class="field">
                  <label class="label">Senha</label>
                  <div class="control">
                    <input 
                      id="password" 
                      type="password" 
                      class="input {{ $errors->has('password') ? ' is-danger' : '' }}" 
                      name="password" 
                      value="{{ old('password') }}" 
                      required>

                    @if ($errors->has('password'))
                    <p class="help">
                      {{ $errors->first('password') }}
                    </p>
                    @endif
                  </div>

                  <div class="field" style="margin-top:20px;">
                    <div class="field-label"></div>
                    <div class="field-body">
                      <div class="field">
                        <div class="control">
                          <button 
                            class="button is-primary" 
                            type="submit" 
                            name="button">Acessar
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>

                </form>
              </div>

            </div> <!-- column -->

            <div class="column">
              <h2 class="subtitle" style="">Como devo acessar?</h2>
              <p class="content">
                O servidor, seja ele, docente ou técnico administrativo
                deve acessar o sistema com as credenciais utilizadas para
                acessar os sistemas instituicionais, ou seja, o servidor
                pode utilizar as mesmas credenciais que utiliza para acessar
                o Sistema Eletrônico de Informações, o SEI.
              </p>
              <h2 class="subtitle">Esqueci minha senha</h2>
              <p class="content">
                Caso o servidor tenha esquecido sua senha, acesse o SUAP 
                <a href="https://suap.ifce.edu.br/comum/solicitar_trocar_senha/" target="_blank">aqui</a>
                e preencha o formulário de recuperação de senha.
              </p>
            </div> <!-- column -->


          </div> <!-- columns -->
        </div> <!-- box -->

      </div>
    </div>
  </div>
@endsection
