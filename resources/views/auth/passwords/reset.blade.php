@extends('layouts.modules')

@section('content')
  <div class="section">
    <div class="container">
      <div class="columns is-centered">
      <div class="column is-half">

          <div class="box">
            <h2 class="title has-text-centered">Cadastre sua nova senha</h2>

              @if (session('status'))
                <div class="alert-primary" alert>
                  {{ session('status') }}
                </div>
              @endif

              <form class="form" role="form" method="POST" action="{{ route('password.request') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">

              <div class="field">
                <label class="label">Endereço de email cadastrado</label>
                <div class="control">
                  <input 
                    id="email" 
                    type="email" 
                    class="input {{ $errors->has('email') ? ' is-danger' : '' }}" 
                    name="email" 
                    value="{{ old('email') }}" 
                    required 
                    autofocus>
                </div>

                @if ($errors->has('email'))
                  <div class="notification">
                    {{ $errors->first('email') }}
                  </div>
                @endif
              </div>

              <div class=field">
                <label class="label">Senha</label>
                <div class="control">
                  <input 
                    id="password" 
                    type="password" 
                    class="input{{ $errors->has('password') ? ' is-danger' : '' }}" 
                    name="password" 
                    value="{{ old('password') }}" 
                    required>
                  </div>

                @if ($errors->has('password'))
                  <div class="notification">
                    {{ $errors->first('password') }}
                  </div>
                @endif
              </div>

              <div class="field">
                <label class="label">Confirmar senha</label>
                <input 
                  id="password_confirmation" 
                  type="password" 
                  class="input{{ $errors->has('password_confirmation') ? ' form-danger' : '' }}" 
                  name="password_confirmation" 
                  value="{{ old('password_confirmation') }}" 
                  required>

                @if ($errors->has('password_confirmation'))
                  <div class="alert-danger" alert>
                    {{ $errors->first('password_confirmation') }}
                  </div>
                @endif
              </div>

              <div class="field">
                <div class="control">
                  <button 
                    class="button is-primary" 
                    type="submit" 
                    name="button">Cadastrar nova senha</button>
                </div>
              </div>
          </form>
        </div>
      </div>
    </div>
    </div>
  </div>
@endsection
