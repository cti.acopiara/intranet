@extends('layouts.modules')

@section('content')
  <div class="section">
    <div class="container">

      <div class="columns is-centered">
      <div class="column is-half">
        <div class="box">
          <h2 class="title has-text-centered">Requisitar nova senha</h2>

            @if (session('status'))
              <div class="notification">
                {{ session('status') }}
              </div>
            @endif

            <form class="form" role="form" method="POST" action="{{ route('password.email') }}">

              {{ csrf_field() }}
              <div class="field">
                <label class="label">Endereço de email cadastrado</label>
                <div class="control">
                  <input 
                    id="email" 
                    type="email" 
                    class="input {{ $errors->has('email') ? ' is-danger' : '' }}" 
                    name="email" 
                    value="{{ old('email') }}" 
                    required 
                    autofocus>
                  </div>

                @if ($errors->has('email'))
                  <div class="notification">
                    {{ $errors->first('email') }}
                  </div>
                @endif
              </div>

              <div class="field">
                <div class="control">
                  <button 
                    class="button is-primary" 
                    type="submit" 
                    name="button">Enviar</button>
                </div>
              </div>
            </form>

        </div>
      </div>
    </div>
    </div>
  </div>
@endsection
