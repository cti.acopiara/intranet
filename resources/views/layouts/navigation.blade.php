<nav class="navbar is-transparent">
  <div class="navbar-brand">
    <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>

  <div id="navbarExampleTransparentExample" class="navbar-menu">
    <div class="navbar-end">

    @if (Auth::guest())
      <div class="navbar-item">
        <div class="field is-grouped">
          <p class="control">
            <a class="bd-tw-button button is-white" href="{{ route('login') }}">
              <span class="icon">
                <i class="mdi mdi-login"></i>
              </span>
              <span>
                Acessar
              </span>
            </a>
          </p>
        </div>
      </div>
    @else
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          {{ Auth::user()->getAttributes()['name'] }}
        </a>
        <div class="navbar-dropdown">
          @if (Auth::user()->role == 'admin')
            <a class="navbar-item" href="{{ route('admin.usuarios.index') }}">Usuários</a>
            <a class="navbar-item" href="{{ route('admin.ramais.index') }}">Ramais</a>
            <a class="navbar-item" href="{{ route('admin.informativos.index') }}">Informativos</a>
            <a class="navbar-item" href="{{ route('admin.links.index') }}">Links</a>
            <a class="navbar-item" href="{{ route('admin.configuracoes.index') }}">Configurações</a>
          @endif
          <a href="{{ route('logout') }}"
            class="navbar-item"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <span class="icon">
              <i class="mdi mdi-logout"></i>
            </span>
            Sair
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </div>
      </div> <!-- has-dropdown -->

    @endif <!-- if auth guest -->
    </div>
  </div>
</nav>


