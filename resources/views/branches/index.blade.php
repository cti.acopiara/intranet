@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">
    <div class="columns">
      <div class="column is-two-thirds">
      <div class="box">
        <h2 class="title">Ramais</h2>
        <div class="field">
          <p class="control has-icons-left">
          <input 
            type="text" 
            class="input is-large"
            id="search-branch" 
            placeholder="Procure o ramal desejado.." 
            title="Type in a name">
            <span class="icon is-left is-large">
              <i class="mdi mdi-magnify"></i>
            </span>
          </p>
        </div>

        <table id="table-branch" class="table table-striped is-fullwidth is-hoverable">
          <thead>
            <tr>
              <th>Setor</th>
              <th>Contatos</th>
              <th>Ramal</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($branches as $branch)
            <tr>
              <td>{{ $branch->sector }}</td>
              <td>{{ $branch->contacts }}</td>
              <td>{{ $branch->branch_line }}</td>
            </tr>
          @endforeach
          </tbody>
        </table>

        <a class="button button-default" href="javascript:history.back()">Voltar</a>
      </div> <!-- box -->
    </div> <!-- column -->

    <div class="column">
      <div class="box">
        <h2 class="title">Informações</h2>
        <div class="content">
          <dl>
            <dt class="has-text-weight-bold">Ligação entre ramais (inclusive Reitoria)</dt>
            <dd>Número desejado</dd>
            <dt class="has-text-weight-bold">Ligação local (DDD 85)</dt>
            <dd>0 + número desejado</dd>
            <dt class="has-text-weight-bold">Ligação DDD (DDD 88, 61, 11 etc)</dt>
            <dd>0 + DDD + número desejado</dd>
            <dt class="has-text-weight-bold">Ligação de Emergência</dt>
            <dd>Digitar os 3 números diretamente</dd>
            <dt class="has-text-weight-bold">Ligação para 0800</dt>
            <dd>Digitar o número diretamente</dd>
          </dl>
        </div>
        <h4 class="subtitle">Atalhos</h4>
        <div class="content">
          <dl>
            <dt class="has-text-weight-bold">Transferir ligação</dt>
            <dd>*2 + ramal desejado</dd>
            <dt class="has-text-weight-bold">Puxar ligação</dt>
            <dd>*8</dd>
          </dl>
        </div>

        <h4 class="subtitle">Ramais da Reitoria</h4>
        <div class="content">
          <p>Os ramais da Reitoria podem ser visualizados <a href="http://ramais.ifce.edu.br" target="_blank">aqui</a>.</p>
        </div> <!-- content -->
      </div>
    </div> <!-- column -->

    </div> <!-- columns -->
  </div> <!-- container -->
</div> <!-- section -->
@endsection


@section('jscontent')
  <script>
  document.querySelector('#search-branch').addEventListener('keyup', searchBranch, false);

  function searchBranch(event) {
      var filter = event.target.value.toUpperCase();
      var rows = document.querySelector("#table-branch tbody").rows;

      for (var i = 0; i < rows.length; i++) {
          var firstCol = rows[i].cells[0].textContent.toUpperCase();
          var secondCol = rows[i].cells[1].textContent.toUpperCase();
          if (firstCol.indexOf(filter) > -1 || secondCol.indexOf(filter) > -1) {
              rows[i].style.display = "";
          } else {
              rows[i].style.display = "none";
          }
      }
  }

  @if(Session::has('success_message'))
    UIkit.notification({
      message: '{!! session('success_message') !!}',
      status: 'primary',
      pos: 'bottom-right',
      timeout: 5000
    });
  @endif

  </script>
@endsection

