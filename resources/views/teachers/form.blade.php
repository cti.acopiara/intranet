<div id="form-docente">
  <div class="uk-animation-fade">

  <div class="uk-margin">
    <label class="uk-form-label">SIAPE</label>
    <input id="docente-login" type="text" class="uk-input" name="login" value="{{ $teacher->login }}" required>

    @if ($errors->has('login'))
      <div class="uk-alert-danger" uk-alert>
        {{ $errors->first('login') }}
      </div>
    @endif
  </div> <!-- uk-margin docente-login -->

  <div class="uk-margin">
    <label class="uk-form-label">Nome completo</label>
    <input id="docente-name" type="text" class="uk-input" name="name" value="{{ $teacher->name }}" required>

    @if ($errors->has('name'))
      <div class="uk-alert-danger" uk-alert>
        {{ $errors->first('name') }}
      </div>
    @endif
  </div> <!-- uk-margin docente-name -->

  <div class="uk-tile ii-tile-warning uk-padding-small">
  <div class="uk-margin">
    <label class="uk-form-label">Carga horária em sala de aula no semestre atual ({{ $setting->current_semester }})</label>
    <input id="docente-workload_in_semester" type="text" class="uk-input" name="workload_in_semester" value="{{ $teacher->meta['workload_in_semester'] }}" required>

    @if ($errors->has('workload_in_semester'))
      <div class="uk-alert-danger" uk-alert>
        {{ $errors->first('workload_in_semester') }}
      </div>
    @endif
  </div> <!-- uk-margin docente-name -->
  </div> <!-- uk-tile -->

  <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
    <label class="uk-form-label">Carga horária</label>
    <label><input class="uk-radio" type="radio" name="workload" value="40" {{ $teacher->meta['workload'] == '40' ? 'checked' : '' }}> 40 horas</label>
    <label><input class="uk-radio" type="radio" name="workload" value="20" {{ $teacher->meta['workload'] == '20' ? 'checked' : '' }}> 20 horas</label>
  </div> <!-- uk-margin workload -->

  <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
    <label class="uk-form-label">Regime</label>
    <label><input class="uk-radio" type="radio" value="Efetivo" name="regime" {{ $teacher->meta['regime'] == 'Efetivo' ? 'checked' : '' }}> Efetivo</label>
    <label><input class="uk-radio" type="radio" value="Substituto" name="regime" {{ $teacher->meta['regime'] == 'Substituto' ? 'checked' : '' }}> Substituto</label>
    <label><input class="uk-radio" type="radio" value="Convidado" name="regime" {{ $teacher->meta['regime'] == 'Convidado' ? 'checked' : '' }}> Convidado</label>
  </div> <!-- uk-margin regime -->

  <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
    <label class="uk-form-label">Cargo</label>
    <label><input class="uk-radio" type="radio" value="Professor" name="responsability" {{ $teacher->meta['responsability'] == 'Professor' ? 'checked' : '' }}> Professor</label>
    <label><input class="uk-radio" type="radio" value="Coordenador" name="responsability" {{ $teacher->meta['responsability'] == 'Coordenador' ? 'checked' : '' }}> Coordenador</label>
    <label><input class="uk-radio" type="radio" value="Diretor" name="responsability" {{ $teacher->meta['responsability'] == 'Diretor' ? 'checked' : '' }}> Diretor</label>
  </div> <!-- uk-margin responsability -->

  <div class="uk-margin">
    <label class="uk-form-label">Data de aniversário</label>
    <input id="docente-birthday" type="text" class="uk-input" name="birthday" value="{{ $teacher->meta['birthday'] }}" required>
  </div> <!-- uk-margin birthday -->

  <div class="uk-margin">
    <label class="uk-form-label">Telefone</label>
    <input id="docente-phone" type="text" class="uk-input" name="phone" value="{{ $teacher->meta['phone'] }}" required>
  </div> <!-- uk-margin docente-phone -->

  <div class="uk-margin">
    <label class="uk-form-label">Departamento</label>
    <input id="docente-department" type="text" class="uk-input" name="department" value="{{ $teacher->meta['department'] }}" required>
  </div> <!--uk-margin docente-department -->

  <div class="uk-margin">
    <label class="uk-form-label">Email</label>
    <input id="docente-email" type="email" class="uk-input" name="email" value="{{ $teacher->email }}" required>
  </div> <!-- uk-margin docoente-email -->

  </div> <!-- uk-animation-fade -->

</div> <!-- form-docente -->

