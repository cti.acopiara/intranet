@extends('layouts.modules')

@section('content')

<div class="uk-section">
  <div class="uk-container uk-container-center">
    <div class="uk-width-1-2@m uk-align-center">
      <div class="uk-card uk-card-body uk-card-small">
        <h2>Editar informações</h2>
        @if ($errors->any())
          <div class="uk-card uk-card-body uk-card-secondary uk-margin">
            <h4 class="uk-card-title">Verifique se o formulário está correto</h4>
            <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
            </ul>
          </div>
        @endif

        <form method="POST" action="{{ route('teachers.teacher.update', $teacher->id) }}" id="edit_teacher_form" name="edit_teacher_form" accept-charset="UTF-8">
          {{ csrf_field() }}

          <input name="_method" type="hidden" value="PUT">
          @include ('teachers.form', ['teacher' => $teacher, 'setting' => $setting])

          <input class="uk-button uk-button-default" type="submit" value="Atualizar">
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
