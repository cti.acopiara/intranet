@extends('layouts.modules')

@section('content')

<div class="uk-section">
  <div class="uk-container uk-container-center">

      @if (count($activities) == 0)
        <div class="uk-child-width-expand@s" uk-grid>

        <div class="uk-width-expand@m uk-text-center">
          <h2>Nenhuma atividade cadastrada</h2>
          <a class="uk-button uk-button-primary ik uk-button-large" href="{{ route('activities.activity.create') }}">
            <span class="uk-icon" uk-icon="icon: plus"></span> CADASTRAR
          </a>
        </div>

        <div class="uk-width-1-3@m">
          <h2>Resumo</h2>
          <div class="uk-card uk-card-default uk-card-body">
            <ul class="uk-list">
              <li><strong>Prof.:</strong> {{ Auth::user()->name }}</li>
              <li><strong>Departamento:</strong> {{ Auth::user()->meta['department'] }}</li>
              <li><strong>CH total:</strong> {{ number_format(Auth::user()->meta['workload'], 2, ',', '.') }}h</li>
              <li><strong>CH em sala de aula ({{ $setting->current_semester }}):</strong> {{ number_format(Auth::user()->meta['workload_in_semester'], 2, ',', '.') }}h</li>
              <li><strong>Regime:</strong> {{ Auth::user()->meta['regime'] }}</li>
            </ul>
            <hr>
            <!-- <p class="uk-text-warning">Em breve, um quadro resumo com as principais informações.</p> -->
          </div>
        </div> <!-- class empty -->
        </div> <!-- uk-grid -->

      @else
        <div class="uk-child-width-expand@s" uk-grid>
        <div class="uk-width-expand@m">
          <h2>Atividades - {{ $setting->current_semester }}</h2>
        <table class="uk-table uk-table-striped uk-table-small uk-table-divider">
          <thead>
            <tr>
              <th>Tipo</th>
              <th>Descrição</th>
              <th>Carga horária</th>
              <th class="uk-table-shrink">Ações</th>
            </tr>
          </thead>

          <tbody>
          @foreach ($activities as $activity)
            <tr>
              <td>{{ $activity->type }}</td>
              <td>{{ $activity->description }}</a></td>
              <td>{{ number_format($activity->workload, 2, ',', '.') }} h</td>
              <td>
                <a href="{{ route('activities.activity.show', $activity->id ) }}" class="uk-icon-link uk-margin-small-right" uk-icon="icon: file"></a>
                <a href="{{ route('activities.activity.edit', $activity->id ) }}" class="uk-icon-link uk-margin-small-right" uk-icon="icon: file-edit"></a>

                <a href="javascript: submitForm()" class="uk-icon-link uk-margin-small-right" uk-icon="icon: trash"></a>

                <form id= "activity-form" name="deleteActivityForm" method="POST" action="{!! route('activities.activity.destroy', $activity->id) !!}" accept-charset="UTF-8">
                  <input name="_method" value="DELETE" type="hidden">
                    {{ csrf_field() }}
                </form>

              </td> 
            </tr>
          @endforeach
            <tr>
              <td></td>
              <td></td>
              <td colspan="2">{{ number_format($activities->sum('workload'), 2, ',', '.') }} h no total</td>
            </tr>

          </tbody>
        </table>
        </div> <!-- class empty -->

        <div class="uk-width-1-3@m">
          <h2>Resumo</h2>
          <div class="uk-card uk-card-default uk-card-body">
            <ul class="uk-list">
              <li><strong>Prof.:</strong> {{ Auth::user()->name }}</li>
              <li><strong>Departamento:</strong> {{ Auth::user()->meta['department'] }}</li>
              <li><strong>CH total:</strong> {{ number_format(Auth::user()->meta['workload'], 2, ',', '.') }}h</li>
              <li><strong>CH em sala de aula ({{ $setting->current_semester }}):</strong> {{ number_format(Auth::user()->meta['workload_in_semester'], 2, ',', '.') }}h</li>
              <li><strong>Regime:</strong> {{ Auth::user()->meta['regime'] }}</li>
            </ul>
            <hr>
            <!-- <p class="uk-text-warning">Em breve, um quadro resumo com as principais informações.</p> -->
          </div>
        </div> <!-- class empty -->

        </div>
      @endif

    </div>
@endsection

@section('jscontent')
  <script>
  function submitForm() {
    var action = confirm("Você tem certeza que deseja remover essa atividade?");
    if (action) {
      var ele = document.getElementById('activity-form');
      ele.submit();
    }

  }
  @if(Session::has('success_message'))
    UIkit.notification({
      message: '{!! session('success_message') !!}',
      status: 'primary',
      pos: 'bottom-right',
      timeout: 5000
    });
  @endif
  </script>
@endsection


