@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">
    <div class="columns">
      <div class="column is-two-thirds">
      <div class="box">
        <h2 class="title">Informativos</h2>

<!--        <div class="field">
          <p class="control has-icons-left">
          <input 
            type="text" 
            class="input is-large"
            id="search-branch" 
            placeholder="Procure o ramal desejado.." 
            title="Type in a name">
            <span class="icon is-left is-large">
              <i class="mdi mdi-magnify"></i>
            </span>
          </p>
        </div>
-->
        <table id="table-branch" class="table table-striped is-fullwidth is-hoverable">
          <thead>
            <tr>
              <th>Data de publicação</th>
              <th>Título</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($informatives as $informative)
            <tr>
              <td>{{ date('d/m/Y', strtotime($informative->published_at)) }}</td>
              <td><a href="{{ $informative->link }}" target="_blank">{{ $informative->title }}</a></td>
            </tr>
          @endforeach
          </tbody>
        </table>

        <a class="button button-default" href="javascript:history.back()">Voltar</a>
      </div> <!-- box -->
    </div> <!-- column -->

    <div class="column">
      <div class="box">
        <h2 class="title">Informações</h2>
        <div class="content">
          <p>Os informativos são produzidos pela Comunicação do Campus Quixadá e enviados
            por e-mail para todos os servidores do Campus.</p>
          <p>Caso você não esteja recebendo-os, entre em contato com o setor de Ccomunicação</p>
        </div>
      </div>
    </div> <!-- column -->

    </div> <!-- columns -->
  </div> <!-- container -->
</div> <!-- section -->
@endsection


@section('jscontent')
  <script>
  document.querySelector('#search-branch').addEventListener('keyup', searchBranch, false);

  function searchBranch(event) {
      var filter = event.target.value.toUpperCase();
      var rows = document.querySelector("#table-branch tbody").rows;

      for (var i = 0; i < rows.length; i++) {
          var firstCol = rows[i].cells[0].textContent.toUpperCase();
          var secondCol = rows[i].cells[1].textContent.toUpperCase();
          if (firstCol.indexOf(filter) > -1 || secondCol.indexOf(filter) > -1) {
              rows[i].style.display = "";
          } else {
              rows[i].style.display = "none";
          }
      }
  }

  @if(Session::has('success_message'))
    UIkit.notification({
      message: '{!! session('success_message') !!}',
      status: 'primary',
      pos: 'bottom-right',
      timeout: 5000
    });
  @endif

  </script>
@endsection

