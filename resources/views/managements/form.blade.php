<div>

  <div class="uk-grid-small" uk-grid>
  <div class="uk-width-1-2@s">

  <div class="uk-margin">
    <div class="uk-form-controls">
      <input id="name" name="name" class="uk-input uk-form-small" type="text" value="{{ $user->name }}" readonly> 
    </div>
  </div>

  <div class="uk-margin">
    <div class="uk-form-controls">
      <input id="login" name="login" class="uk-input uk-form-small" type="text" value="{{ $user->login }}" readonly> 
    </div>
  </div>

  </div> <!-- uk-width -->

  <div class="uk-width-1-2@s">

  <div class="uk-margin">
    <div class="uk-form-controls">
      <input id="responsability" name="responsability" class="uk-input uk-form-small" type="text" value="{{ $user->meta['responsability'] }}" readonly> 
    </div>
  </div>

  <div class="uk-margin">
    <div class="uk-form-controls">
      <input id="sector" name="sector" class="uk-input uk-form-small" type="text" value="{{ $user->meta['sector'] }}" readonly> 
    </div>
  </div>

  </div> <!-- uk-width -->
  </div> <!-- uk-grid -->

  <div class="uk-margin">
  <label class="uk-form-label" for="role">Função <span class="uk-text-small uk-text-warning">Caso não tenha, deixar em branco</span></label>
    <div class="uk-form-controls">
      <input id="role" name="role" class="uk-input uk-form-small" type="text" value=""> 
    </div>
  </div>

  <div class="uk-grid-small" uk-grid>

    <div class="uk-width-1-2@s">
      <label class="uk-form-label" for="month">Mês</label>
      <div class="uk-form-controls">
        <select id="month" name="month" class="uk-select uk-form-small">
          <option value="1">Janeiro</option>
          <option value="2">Fevereiro</option>
          <option value="3">Março</option>
          <option value="4">Abril</option>
          <option value="5">Maio</option>
          <option value="6">Junho</option>
          <option value="7">Julho</option>
          <option value="8">Agosto</option>
          <option value="9">Setembro</option>
          <option value="10">Outubro</option>
          <option value="11">Novembro</option>
          <option value="12">Dezembro</option>
        </select>
      </div>
    </div>

    <div class="uk-width-1-2@s">
      <label class="uk-form-label" for="Ano">Ano</label>
      <div class="uk-form-controls">
        <select id="year" name="year" class="uk-select uk-form-small">
          <option value="2018">2018</option>
          <option value="2019">2019</option>
          <option value="2020">2020</option>
          <option value="2021">2021</option>
          <option value="2022">2022</option>
          <option value="2023">2023</option>
        </select>
      </div>
    </div>
  </div>

  <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
    <label class="uk-form-label">Jornada de trabalho semanal</label>
    <label><input class="uk-radio" type="radio" name="workload" value="40" checked onchange="toggleShift(this)"> 40 horas</label>
    <label><input class="uk-radio" type="radio" name="workload" value="30" onchange="toggleShift(this)"> 30 horas</label>
  </div>

  <div id="form-shift" hidden>
  <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
    <label class="uk-form-label">Turno</label>
    <label><input class="uk-radio" type="radio" name="shift" value="m" checked> Manhã</label>
    <label><input class="uk-radio" type="radio" name="shift" value="t"> Tarde</label>
  </div>
  </div> <!-- shift-form -->

  <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
    <label class="uk-form-label">Férias?</label>
    <label><input class="uk-radio" type="radio" name="vacation" value="n" onchange="toggleShift(this)" checked> Não</label>
    <label><input class="uk-radio" type="radio" name="vacation" value="s" onchange="toggleShift(this)"> Sim</label>
  </div>

  <div id="form-vacation" hidden>
  <div class="uk-margin uk-grid-small uk-grid" uk-grid>

    <div class="uk-width-1-2@s">
      <label class="uk-form-label" for="start-vacation">Início</label>
        <div class="uk-form-controls">
          <input id="vacation-1" name="start-vacation" class="uk-input uk-form-small" type="text" value=""> 
        </div>
    </div>

    <div class="uk-width-1-2@s">
      <label class="uk-form-label" for="end-vacation">Término</label>
        <div class="uk-form-controls">
          <input id="vacation-2" name="end-vacation" class="uk-input uk-form-small" type="text" value=""> 
        </div>
    </div>

    </div> <!-- uk-grid -->
    </div> <!-- vacation-form -->


</div>

@section('jscontent')
<script>
  function toggleShift(that) {
    if (that.value == "30") {
      document.getElementById("form-shift").removeAttribute("hidden");
    } else if (that.value == "40") {
      document.getElementById("form-shift").setAttribute("hidden", true);
    } else if (that.value == "s") {
      document.getElementById("form-vacation").removeAttribute("hidden");
    } else if (that.value == "n") {
      document.getElementById("form-vacation").setAttribute("hidden", true);
    }
  }

  </script>
@endsection
