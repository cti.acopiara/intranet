@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">
    <div class="">
      <div class="box">
        <h2 class="title">Usuários</h2>

            <table class="table table-striped is-fullwidth is-hoverable">
              <thead>
                <tr>
                  <th>Tipo</th>
                  <th>Nome</th>
                  <th>Acesso</th>
                  <th class="uk-table-shrink">Ações</th>
                </tr>
              </thead>

              <tbody>
              @foreach ($users as $user)
                <tr>
                  <td>{{ $user->user_type }}</td>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->role }}</td>
                  <td>
                    <a href="{{ route('admin.usuarios.edit', $user ) }}" class="">
                      <span class="icon">
                        <i class="mdi mdi-pencil"></i>
                      </span>
                    </a>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          <a class="button button-default" href="javascript:history.back()">Voltar</a>
      </div>
    </div>
  </div>
@endsection

@section('jscontent')
  <script>
  @if(Session::has('success_message'))
    UIkit.notification({
      message: '{!! session('success_message') !!}',
      status: 'primary',
      pos: 'bottom-right',
      timeout: 5000
    });
  @endif
  </script>
@endsection


