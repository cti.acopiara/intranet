
<div id="form-docente" class="columns">
  <div class="column">

  <div class="field">
    <label class="label">SIAPE</label>
    <input id="docente-login" type="text" class="input" name="username" value="{{ $user->username}}" disabled>

    @if ($errors->has('username'))
      <div class="is-danger">
        {{ $errors->first('username') }}
      </div>
    @endif
  </div> <!-- field docente-login -->

  <div class="field">
    <label class="label">Nome completo</label>
    <input id="docente-name" type="text" class="input" name="name" value="{{ $user->name }}" disabled>

    @if ($errors->has('name'))
      <div class="is-danger">
        {{ $errors->first('name') }}
      </div>
    @endif
  </div> <!-- field docente-name -->

  <div class="field uk-grid-small uk-child-width-auto uk-grid">
    <label class="label">Acesso</label>
    <label><input class="uk-radio" type="radio" value="comum" name="role" {{ $user->role == 'comum' ? 'checked' : '' }}> Comum</label>
    <label><input class="uk-radio" type="radio" value="coordenador" name="role" {{ $user->role  == 'coordenador' ? 'checked' : '' }}> Coordenador</label>
    <label><input class="uk-radio" type="radio" value="gestor" name="role" {{ $user->role == 'gestor' ? 'checked' : '' }}> Gestor</label>
    <label><input class="uk-radio" type="radio" value="admin" name="role" {{ $user->role == 'admin' ? 'checked' : '' }}> Administrador</label>
  </div> <!-- field responsability -->

  </div> <!-- uk-animation-fade -->

</div> <!-- form-docente -->

