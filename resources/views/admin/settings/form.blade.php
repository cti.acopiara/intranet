<div class="columns">
  <div class="column">

  <div class="field">
    <label class="label">Semestre atual</label>
    <input id="current-semester" type="text" class="input {{ $errors->has('current_semester') ? 'is-danger' : '' }}" name="current_semester" value="{{ old('current_semester', optional($setting)->current_semester) }}" required>

    @if ($errors->has('current_semester'))
      <div class="is-dangerqg">
        {{ $errors->first('current_semester') }}
      </div>
    @endif
  </div>

  <div class="field">
    <label class="label">Primeiro dia letivo</label>
    <input id="first-school-day" type="text" class="input {{ $errors->has('first-school-day') ? 'is-danger' : '' }}" name="first_school_day" value="{{ old('first_school_day', optional($setting)->first_school_day) }}" required>

    @if ($errors->has('first_school_day'))
      <div class="is-dangerqg">
        {{ $errors->first('first_school_day') }}
      </div>
    @endif
  </div>

  <div class="field">
    <label class="label">Último dia letivo</label>
    <input id="last-school-day" type="text" class="input {{ $errors->has('last-school-day') ? 'is-danger' : '' }}" name="last_school_day" value="{{ old('last_school_day', optional($setting)->last_school_day) }}" required>

    @if ($errors->has('last_school_day'))
      <div class="is-dangerqg">
        {{ $errors->first('last_school_day') }}
      </div>
    @endif
  </div>

  </div>
</div> <!-- uk-animation-fade -->

