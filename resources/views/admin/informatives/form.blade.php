<div class="columns">
  <div class="column">

    <div class="field">
      <label class="label" for="published_at">Data de publicação</label>
      <div class="control">
        <input class="input {{ $errors->has('published_at') ? 'is-danger' : '' }}" name="published_at" type="text" id="sector" value="{{ old('published_at', optional($informative)->published_at) }}" minlength="1" maxlength="30" required="true" placeholder="Data do informativo">
        {!! $errors->first('published_at', '<p class="is-small is-danger">:message</p>') !!}
      </div>
    </div>

    <div class="field">
      <label class="label" for="title">Título</label>
      <div class="control">
        <input class="input {{ $errors->has('title') ? 'is-danger' : '' }}" name="title" type="text" id="title" value="{{ old('title', optional($informative)->title) }}" minlength="10" maxlength="30" required="true" placeholder="Título">
        {!! $errors->first('sector', '<p class="is-small is-danger">:message</p>') !!}
      </div>
    </div>

    <div class="field">
      <label class="label" for="link">Link</label>
      <div class="control">
        <input class="input {{ $errors->has('link') ? 'is-danger' : '' }}" name="link" type="text" id="link" value="{{ old('link', optional($informative)->link) }}" minlength="1" required="true" placeholder="Link">
        {!! $errors->first('sector', '<p class="is-small is-danger">:message</p>') !!}
      </div>
    </div>

  </div>
</div>
