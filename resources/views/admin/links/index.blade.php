@extends('layouts.modules')

@section('content')

<div class="section">
  <div class="container">
    <div class="">
      <div class="box">

        <div id="notification" class="notification is-success is-light is-hidden">
            {{ session('success_message') }}
        </div>

        <h2 class="title">Links</h2>
        <p><a class="button is-info" href="{{ route('admin.links.create') }}">Adicionar novo link</a></p>

        @if (count($links) == 0)
          <p class="uk-text-center">Nenhum link cadastrado</p>
        @else

        <table class="table table-striped is-fullwidth is-hoverable">
          <thead>
            <tr>
              <th>Link</th>
              <th>Descrição</th>
              <th>Ações</th>
            </tr>
          </thead>

          <tbody>
            @foreach ($links as $link)
            <tr>
              <td>{{ $link->link }}</td>
              <td>{{ $link->description }}</td>
              <td>
                <a href="{{ route('admin.links.edit', $link->id ) }}" class="button is-white is-text">
                  <span class="icon">
                    <i class="mdi mdi-pencil"></i>
                  </span>
                </a>
                <form id="link-form" class="is-pulled-left" name="deleteLinkForm" method="POST" action="{!! route('admin.links.destroy', $link) !!}" accept-charset="UTF-8">
                    <input name="_method" value="DELETE" type="hidden">
                    {{ csrf_field() }}
                    <button class="button is-white is-text" type="submit">
                        <span class="icon">
                            <i class="mdi mdi-delete"></i>
                        </span>
                    </button>
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>

        <a class="button button-default" href="javascript:history.back()">Voltar</a>
        @endif
      </div>
    </div>
  </div> <!-- container -->
</div> <!-- /section -->
@endsection

@section('jscontent')
  <script>
  @if(Session::has('success_message'))
    var notification = document.getElementById('notification');
    notification.classList.remove('is-hidden');
    setTimeout(function(){ notification.classList.add('is-hidden'); }, 5000);
  @endif
  </script>
@endsection

