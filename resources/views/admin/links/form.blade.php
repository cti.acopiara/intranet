<div class="columns">
  <div class="column">

    <div class="field">
      <label class="label" for="link">Link</label>
      <div class="control">
        <input class="input {{ $errors->has('link') ? 'is-danger' : '' }}" name="link" type="text" id="link" value="{{ old('link', optional($link)->link) }}" minlength="1" maxlength="191" required="true" placeholder="Link completo (http://...)">
        <span class="help">Link no formato completo, http://.. ou https://..)</span>
        {!! $errors->first('sector', '<p class="is-small is-danger">:message</p>') !!}
      </div>
    </div>

    <div class="field">
      <label class="label" for="description">Descrição</label>
      <div class="control">
        <input class="input {{ $errors->has('description') ? 'is-danger' : '' }}" name="description" type="text" id="description" value="{{ old('description', optional($link)->description) }}" minlength="10" maxlength="50" required="true" placeholder="Descrição (exemplo: Suporte DGTI)">
        {!! $errors->first('sector', '<p class="is-small is-danger">:message</p>') !!}
      </div>
    </div>
  </div>
</div>
