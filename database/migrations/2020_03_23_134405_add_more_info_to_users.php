<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreInfoToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('sector')->nullable();
            $table->string('responsability')->nullable();
            $table->string('work_schedule')->nullable();
            $table->string('work_load')->nullable();
            $table->string('work_shift')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('sector');
            $table->dropColumn('responsability');
            $table->dropColumn('work_schedule');
            $table->dropColumn('work_load');
            $table->dropColumn('work_shift');
        });
    }
}
