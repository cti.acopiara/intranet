<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class SampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $faker = Faker::create('pt_BR');
      foreach (range(1,40) as $index) {
        $user = new User;
        $user->name = $faker->name;
        $user->login = $faker->unique()->numberBetween(2, 100);
        $user->user_type = 'docente';
        $user->email = $faker->email;
        $user->password = bcrypt('123456');
        $user->meta = [
          'modules' => 'docente',
          'birthday' => $faker->dateTimeThisCentury,
          'regime' => 'Efetivo',
          'exclusive_dedication' => 'Sim',
          'phone' => $faker->phoneNumber,
          'workload' => $faker->randomElement(['40', '20']),
          'workload_in_semester' => $faker->randomDigitNotNull,
          'responsability' => 'Professor',
          'department' => 'Fictício'
        ];
        $user->save();
      }

    }
}
