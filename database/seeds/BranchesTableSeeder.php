<?php

use App\Branch;
use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      for ($i = 1; $i <= 50; $i++) {
        $line = new Branch;
        $line->sector = base64_encode(random_bytes(10)); 
        $line->contacts = base64_encode(random_bytes(10));
        $line->branch_line = base64_encode(random_bytes(3));
        $line->save();
      }

    }
}
