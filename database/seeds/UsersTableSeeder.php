<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $admin = new User;
      $admin->name = 'Suporte CTI';
      $admin->user_type = 'admin';
      $admin->login = '1';
      $admin->email = 'suporte.quixada@ifce.edu.br';
      $admin->password = bcrypt('123456');
      $admin->role = 'admin';
      $admin->meta = [
          'modules' => 'all',
          'birthday' => '01/01/1970',
          'exclusive_dedication' => 'Sim',
          'workload' => '40',
          'regime' => 'Não se aplica',
          'responsability' => 'Técnico',
          'department' => 'CTI',
      ];
      $admin->save();

    }
}
