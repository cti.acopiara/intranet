<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {

    $faker->addProvider(new \Faker\Provider\pt_BR\PhoneNumber($faker));

    return [
        'name' => $faker->name,
        'student_registration' => $faker->isbn13,
        'email_personal' => $faker->safeEmail,
        'phone_1' => $faker->cellphoneNumber(false),
        'campus' => $faker->city
    ];
});
